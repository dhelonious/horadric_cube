if i3 then
  i3.register_craft_type("transmute", {
    description = S("Transmute"),
    icon = "horadric_cube_icon_transmute.png",
  })
end


horadric_cube = {
  recipes = {
    --[[
    Recipe example transmute sapling to pine_sapling
    {
      input = "default:sapling",
      output = "default:pine_sapling",
    }
    --]]
  },
  init = function(pos)
    local meta = minetest.get_meta(pos)
    meta:set_string("formspec", horadric_cube.get_formspec())
    meta:set_string("infotext", S("Horadric cube"))
  end,
  get_recipe_inputs = function(self)
    local result = {}

    for i, n in ipairs(self.recipes) do
      result[n.input] = true
    end

    return result
  end,
  get_craft_result = function(input_stack)
    for i, n in ipairs(horadric_cube.recipes) do
      local recipe_stack = ItemStack(n.input)

      if input_stack:get_name() == recipe_stack:get_name() then
        local r_count = recipe_stack:get_count()
        local i_count = input_stack:get_count()

        if i_count >= r_count and (i_count % r_count) == 0 then
          local output_stack = ItemStack(n.output)
          local o_count = i_count / r_count
          output_stack:set_count(output_stack:get_count() * o_count)
          return output_stack
        end
      end
    end

    return nil
  end,
  register_craft = function(self, recipe_def)
    local existing_recipe = self.get_craft_result(ItemStack(recipe_def.input))
    if existing_recipe then
      if existing_recipe.output ~= recipe_def.output then
        table.insert(self.recipes, recipe_def)
      end
    else
      table.insert(self.recipes, recipe_def)
    end

    if not recipe_def.hidden and i3 then
      i3.register_craft {
        type = "transmute",
        result = recipe_def.output,
        items = {recipe_def.input},
      }
    end
  end,
  get_formspec = function()
    return "size[8,8]"..
    "list[context;input;3.5,1;1,1]"..
    "image_button[3.5,2;1,1;horadric_cube_icon_transmute.png;transmuteBtn;]" ..
    "tooltip[3.5,2;1,1;"..
    S("Transmute")..
    "]"..
    "list[current_player;main;0,4;8,4]"
  end
}


local function allow_metadata_inventory_put(pos, listname, index, stack, player)
  if listname == "output" then
    return 0
  end

  if horadric_cube:get_recipe_inputs()[stack:get_name()] ~= nil then
    return stack:get_count()
  end

  return 0
end


minetest.register_node("horadric_cube:cube", {
  description = S("Horadric cube"),
  tiles = {
    "horadric_cube_cube.png",
  },
  stack_max = 1,
  groups = {
    choppy = 3,
    oddly_breakable_by_hand = 2,
  },
  on_construct = function(pos)
    local meta = minetest.get_meta(pos)
    local inv = meta:get_inventory()
    inv:set_size("input", 1)
    meta:set_string("infotext", S("Horadric cube"))
    meta:set_string("formspec", horadric_cube.get_formspec())
    horadric_cube.init(pos)
  end,
  on_receive_fields = function(pos, formname, fields, sender)
    local meta = minetest.get_meta(pos)
    local inv = meta:get_inventory()

    if fields["transmuteBtn"] then
      if inv:is_empty("input") then
        return
      end

      local inputStack = inv:get_stack("input", 1)
      local recipe = horadric_cube.get_craft_result(inputStack)

      if recipe ~= nil then
        inv:remove_item("input", ItemStack(inputStack))
        inv:set_stack("input", 1, recipe)
      end
    end
  end,
})


return horadric_cube
