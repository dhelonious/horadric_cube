# Horadric Cube mod

Mod adds legendary artifact that can transmute items

No other mods dependencies

## Recipes

You can register own transmute recipe

```horadric_cube.register_craft(craftDef)```

where **craftDef** is table with next structure:

```
{
  hidden = false,
  input = <ItemStackString>,
  output = <ItemStackString>,
}
```

Also visible (hidden=false) recipes automatically register in [i3](https://content.minetest.net/packages/jp/i3/). if mod installed


## Examples:

```
horadric_cube:register_craft({
  input = "default:coal 9",
  output = "default:diamond"
})

horadric_cube:register_craft({
  input = "default:tree",
  output = "default:coal"
})
```
